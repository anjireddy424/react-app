import React from 'react';
// import './App.css';
import Counter from './Counter';
class App extends React.Component {
       state={
         count:0
       };
increment =()=>{
  this.setState({
    count:this.state.count+1
  });
};
decrement =() =>{
  this.setState({
    count:this.state.count-1
  });
};

  render() 
  {
    return (
      <div>
        {/* <Counter /> */}
      <Counter
      count={this.state.count}
      decrement={this.decrement}
      increment={this.increment}
      />
      <Counter
      count={this.state.count}
      decrement={this.decrement}
      increment={this.increment}
     />
     </div>
    );
  }
}
export default App;
